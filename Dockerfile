FROM python:3.8.18-alpine3.18

WORKDIR /app

RUN apk update && \
    apk add --no-cache netcat-openbsd

COPY . . 

ENTRYPOINT ["python3","-u", "server.py"]